package com.cleverit.springboottest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cleverit.springboottest.controllers.UserController;
import com.cleverit.springboottest.entity.models.Auto;
import com.cleverit.springboottest.entity.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserServiceImplTest {
	
	private static final String URL="http://arsene.azurewebsites.net/User";
	
	@Test
	public void readtest() {
	ObjectMapper mapper = new ObjectMapper();
	try {
		mapper.readTree(new URL(URL));
	} catch (IOException e) {
		e.printStackTrace();
	}
	}
	
	@Test
	public void getonetest() {
		ObjectMapper mapper = new ObjectMapper();
		final String userid ="J1abc234";
		try {
			Auto[] usrPost = mapper.readValue(new URL(URL), Auto[].class);
			for (Auto itr : usrPost) {
				if(itr.getId().equals(userid))
					break;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
