package com.cleverit.springboottest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cleverit.springboottest.entity.models.User;

import com.cleverit.springboottest.entity.services.IUserService;


@RestController
public class UserController {
	
	public static final String URL = "http://arsene.azurewebsites.net/User";
	
	@Autowired
	IUserService userservice;
	
	@GetMapping(URL)
	public List<User> getAllItem() {
		return userservice.getAll();
	}
	
	@GetMapping(URL+"/{id}")
	public User getOne(@PathVariable(value = "id") String id ) {
		return userservice.get(id);
	}
	
	@PostMapping(URL)
	public void add(User user) {
		userservice.post(user);
	}
	
	@PutMapping(URL)
	public void update(User user, String id) {
		userservice.put(user, id);
	}
	
	@DeleteMapping(URL+"/{id}")
	public void delete(String id) {
		userservice.delete(id);
	}

}
