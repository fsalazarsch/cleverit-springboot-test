package com.cleverit.springboottest.controllers;

import java.io.IOException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cleverit.springboottest.entity.models.Auto;
import com.cleverit.springboottest.entity.services.IAutoService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class AutoController {
	
	public static final String URL ="https://arsene.azurewebsites.net/LicensePlate";

	@Autowired
	IAutoService autoservice;
	
	@GetMapping("/autos")
	public String getAllItem() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String res = "";
			Auto[] usrPost = mapper.readValue(new URL(URL), Auto[].class);
			for (Auto itr : usrPost) {
				autoservice.post(itr);
			    res += itr.getId();
			    res +="|"+itr.getPatente();
			    res +="|"+itr.getTipoAuto();
			    res +="|"+itr.getColor();
			    res +="|"+itr.getTest()+"<br>";
			}
			return res;
		} catch (IOException e) {
			e.printStackTrace();
			return "error";
		
		}

	}
}
