package com.cleverit.springboottest.entity.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.cleverit.springboottest.entity.dao.IUserDao;
import com.cleverit.springboottest.entity.models.User;

@Repository
public class UserServiceImpl implements IUserService {
	
	@Autowired
	private IUserDao userDao;
	
	@Override
	public User get(String id) {
		return userDao.findById(id).get();
	}

	@Override
	public List<User> getAll() {
		return (List <User>) userDao.findAll();
	}

	@Override
	public void post(User user) {
		userDao.save(user);
		
	}

	@Override
	public void put(User user, String id) {
		userDao.findById(id).ifPresent((x)-> {
			user.setId(id);
			userDao.save(user);
		});
		
	}

	@Override
	public void delete(String id) {
		userDao.deleteById(id);
		
	}


}
