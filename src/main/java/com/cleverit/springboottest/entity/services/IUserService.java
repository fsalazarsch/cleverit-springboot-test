package com.cleverit.springboottest.entity.services;
import com.cleverit.springboottest.entity.models.User;

import java.util.List;

public interface IUserService {

	public User get(String id);
	public List<User> getAll();
	public void post(User item);
	public void put(User item, String id);
	public void delete(String id);
}
