package com.cleverit.springboottest.entity.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cleverit.springboottest.entity.dao.IAutoDao;
import com.cleverit.springboottest.entity.models.Auto;

@Repository
public class AutoServiceImpl implements IAutoService{

	@Autowired
	private IAutoDao autoDao;
	
	@Override
	public Auto get(String id) {
		return autoDao.findById(id).get();
	}

	@Override
	public List<Auto> getAll() {
		return (List <Auto>) autoDao.findAll();
	}

	@Override
	public void post(Auto auto) {
		autoDao.save(auto);
		
	}

	@Override
	public void put(Auto auto, String id) {
		autoDao.findById(id).ifPresent((x)-> {
			auto.setId(id);
			autoDao.save(auto);
		});
		
	}

	@Override
	public void delete(String id) {
		autoDao.deleteById(id);
		
	}

}
