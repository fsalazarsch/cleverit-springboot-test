package com.cleverit.springboottest.entity.services;

import java.util.List;

import com.cleverit.springboottest.entity.models.Auto;

public interface IAutoService {
	public Auto get(String id);
	public List<Auto> getAll();
	public void post(Auto item);
	public void put(Auto item, String id);
	public void delete(String id);
}
