package com.cleverit.springboottest.entity.dao;

import org.springframework.data.repository.CrudRepository;
import com.cleverit.springboottest.entity.models.Auto;

public interface IAutoDao extends CrudRepository<Auto, String> {

}
