package com.cleverit.springboottest.entity.dao;

import org.springframework.data.repository.CrudRepository;
import com.cleverit.springboottest.entity.models.User;

public interface IUserDao extends CrudRepository<User, String> {

}
