# README #


### Estructura de tabla auto ###

-- Dumping structure for table test.auto
CREATE TABLE IF NOT EXISTS `auto` (
  `id` varchar(50) NOT NULL,
  `patente` varchar(50) DEFAULT NULL,
  `tipo_auto` varchar(50) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `test` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;